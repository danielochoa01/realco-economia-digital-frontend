export interface Usuario {
    _id?: any;
    nombre?: string;
    tipoUsuario?: number;
    email?: string;
    numeroIdentidad?: string;
    RTN?: string;
    estadoCivil?: string;
    imgPerfil?: string;
    fechaNacimiento?: Date | string;
    fechaRegistro?: Date |string;
    lugarOrigen?: Date;
    genero?: string;
    telefono?: string;
    contrasena?: string;
}
