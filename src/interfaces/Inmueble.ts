export interface InmuebleCard {
    precio: number;
    tipo: string;
    ubicacion: {
        departamento: string;
        municipio: string;
        colonia: string;
        NumeroCasa: number;
        bloque: number;
    };
    fotos: [{ url: string }];
}

export interface Inmueble {
    precio: number;
    tamano: number;
    tipo: string;
    contiene: {
        habitaciones: number;
        banos: number;
    };
    servicios: {
        tv: boolean;
        luz: boolean;
        agua: boolean;
    };
    fotos: [{
        url: string;
        nombre: string;
        descripcion: string;
    }];
     requisitos: {
        trabajo: boolean;
        estadoCivil: string;
        personasPermitidas: number;
    };
    ubicacion: {
        departamento: string;
        municipio: string;
        colonia: string;
        NumeroCasa: number;
        bloque: number;
    };
}
