import Vue from 'vue';
import Router from 'vue-router';
import Landing from '@/views/Landing.vue';
import RutasHome from '@/routes/home';
import RutasUsuario from '@/routes/usuarios';
Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '',
      name: 'landing',
      component: Landing,
    },
    {
      path: '/home',
      component: () => import(/* webpackChunkName: "Home" */ '@/views/Home.vue'),
      children: RutasHome,
    },
    {
      path: '/perfil',
      component: () => import(/* webpackChunkName: "Perfil" */ '@/views/Perfil.vue'),
    },
    {
      path: '/usuarios',
      component: () => import(/* webpackChunkName: "Usuarios" */ '@/views/Usuarios.vue'),
      children: RutasUsuario,
    },
  ],
});
