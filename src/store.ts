import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    usuario: {},
  },
  mutations: {
    usuario(state, usuario) {
      state.usuario = usuario;
    },
  },
  actions: {
    datos(mutaciones, {_id, email, nombre, tipoUsuario, imgPerfil}) {
      if ( imgPerfil === '/assets/profile.jpg') {
        imgPerfil = imgPerfil;
      } else {
        imgPerfil = imgPerfil;
      }
      mutaciones.commit('usuario', {_id, email, nombre, tipoUsuario, imgPerfil});
    },
    logout(mutaciones) {
      mutaciones.commit('usuario', {});
    },

    datosCompletos(mutaciones, {
          nombre,
          tipoUsuario,
          email,
          numeroIdentidad,
          RTN,
          estadoCivil,
          imgPerfil,
          fechaNacimiento,
          fechaRegistro,
          genero,
          telefono }) {
      if ( imgPerfil === '/assets/profile.jpg') {
        imgPerfil = imgPerfil;
      } else {
        imgPerfil = imgPerfil;
      }
      mutaciones.commit('usuario', {
        nombre,
        tipoUsuario,
        email,
        numeroIdentidad,
        RTN,
        estadoCivil,
        imgPerfil,
        fechaNacimiento,
        fechaRegistro,
        genero,
        telefono });
    },
  },
});
