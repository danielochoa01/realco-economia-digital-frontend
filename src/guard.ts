export function beforeRouteEnter(to: object, from: object , next: Function) {
    // called before the route that renders this component is confirmed.
    // does NOT have access to `this` component instance,
    // because it has not been created yet when this guard is called!
      next((vm: any) => {
      // access to component instance via `vm`
      if (vm.$store.state.usuario.email) {
        next();
      } else {
        alert(`Necesita estar logueado para acceder a ${to.path}`);
        next(from.path);
      }
    });
}