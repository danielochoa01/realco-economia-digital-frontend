import Vue from 'vue';
import Router from 'vue-router';
import Login from '@/components/usuarios/Login.vue';
Vue.use(Router);

export default [
  {
    path: '',
    name: 'login',
    component: Login,
  },
  {
    path: '/registro',
    name: 'registro',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '@/components/usuarios/Registro.vue'),
  },
  {
    path: '/login',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: Login,
  },
];
