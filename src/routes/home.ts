import listInmuebles from '@/components/home/InmueblesHome.vue';

export default [
  {
    path: '',
    name: 'listInmuebles',
    component: listInmuebles,
  },
  // {
  //   path: '/about',
  //   name: 'about',
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () => import(/* webpackChunkName: "about" */ '@/views/About.vue'),
  // },
  {
    path: '/inmueble/:id',
    name: 'inmueble',
    component: () => import(/* webpackChunkName: "inmueble" */ '@/components/home/Inmueble.vue'),
  },
  {
    path: '/busqueda/:palabras',
    name: 'busqueda',
    component: () => import(/* webpackChunkName: "inmueble" */ '@/components/home/Busqueda.vue'),
  },
  {
    path: '/favoritos',
    name: 'favoritos',
    component: () => import(/* webpackChunkName: "inmueble" */ '@/components/home/Favoritos.vue'),
  },
  {
    path: '/inmublespropios',
    name: 'inmublespropios',
    component: () => import(/* webpackChunkName: "inmueble" */ '@/components/home/InmublesUsuario.vue'),
  },
];
