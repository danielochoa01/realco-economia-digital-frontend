export default {
  numeroRules: [
    v => !!v || 'El campo es obligatorio',
    v => !/(\D)/.test(v) || 'El campo tiene que ser un numero'
  ],
  obligatorioRules: [
    v => !!v || 'El campo es obligatorio'
  ],
  numNoOblRules: [
    v => !/(\D)/.test(v) || 'El campo tiene que ser un numero'
  ],
  banhosRules: [
    v => !!v || 'El campo es obligatorio',
    v => !/(\D)/.test(v) || (!/((\.[012346789])$)/.test(v) && !/((\..*){2,})/.test(v) )||'El campo tiene que ser un numero',
  ],
}