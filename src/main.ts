import Vue from 'vue';
import '@/plugins/vuetify';
import App from '@/App.vue';
import router from '@/route';
import store from '@/store';
import moment from 'moment';

Vue.config.productionTip = false;

Vue.filter('formatDate', (value: string) => {
  if (value) {
    return moment.utc(String(value)).format('DD/MM/YYYY');
  }
});

new Vue({
  store,
  router,
  render: (h) => h(App),
}).$mount('#app');
