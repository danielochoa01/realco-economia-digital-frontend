# Realco Economia Digital

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Para que funcione la busqueda
Ejecutar en mongodb
```
db.inmuebles.createIndex({"ubicacion.departamento":"text","ubicacion.municipio":"text","ubicacion.colonia":"text"})
```
